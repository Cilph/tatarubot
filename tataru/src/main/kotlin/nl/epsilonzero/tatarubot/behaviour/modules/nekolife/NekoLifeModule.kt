package nl.epsilonzero.tatarubot.behaviour.modules.nekolife

import io.ktor.client.HttpClient
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.request.get
import nl.epsilonzero.tatarubot.framework.ModuleBuilder
import nl.epsilonzero.tatarubot.framework.ModulesBuilder
import nl.epsilonzero.tatarubot.framework.common.effects.resolveUser
import nl.epsilonzero.tatarubot.framework.common.effects.respond
import nl.epsilonzero.tatarubot.framework.entities.User
import java.awt.Color

fun ModulesBuilder<*, *>.nekosLife() = module("nekoslife") {
    val httpClient = HttpClient {
        install(JsonFeature) {
            serializer = JacksonSerializer()
        }
    }

    enumValues<NekosLifeVerb>().forEach {
        nekoLifeVerb(it, httpClient::fetchVerbImage)
    }
}

internal enum class NekosLifeVerb(
    val verb: String,
    val printer: (User, User) -> String
) {
    TICKLE("tickle",
        { sub, obj -> "**${sub.userName}** gave **${obj.userName}** a firm tickle." }),
    POKE("poke",
        { sub, obj -> "**8${sub.userName}** gives **${obj.userName}** a poke." }),
    KISS("kiss",
        { sub, obj -> "**${sub.userName}** gives **${obj.userName}** one on the cheek... or mouth." }),
    SLAP("slap",
        { sub, obj -> "**${sub.userName}** slaps **${obj.userName}** with a trout." }),
    CUDDLE("cuddle",
        { sub, obj -> "**${sub.userName}** cuddles **${obj.userName}**." }),
    HUG("hug",
        { sub, obj -> "**${sub.userName}** gives **${obj.userName}** a loving embrace." }),
    PAT("pat",
        { sub, obj -> "**${sub.userName}** gave **${obj.userName}** a loving pat." }),
    FEED("feed",
        { sub, obj -> "**${sub.userName}** feeds **${obj.userName}** a cookie." }),
}

internal fun ModuleBuilder<*, *>.nekoLifeVerb(
    verb: NekosLifeVerb,
    fetchImageUrl: suspend (NekosLifeVerb) -> String
) = command(verb.verb) { args ->
    val author = args.author ?: return@command
    val target = args.userMentionIds.firstOrNull()?.let { resolveUser(it) } ?: return@command
    val verbImage = fetchImageUrl(verb)

    val message = respond(args) {
        embed {
            color = Color(0xDD2A3D)
            description = verb.printer(author, target)
            imageUrl = verbImage
        }
    }
}

data class NekosLifeImageResponse(val url: String)

internal suspend fun HttpClient.fetchVerbImage(verb: NekosLifeVerb): String {
    return get<NekosLifeImageResponse>("https://nekos.life/api/v2/img/${verb.verb}").url
}