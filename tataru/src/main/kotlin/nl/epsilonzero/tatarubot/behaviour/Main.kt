package nl.epsilonzero.tatarubot.behaviour

import TataruBot
import kotlinx.coroutines.coroutineScope
import mu.KotlinLogging

private val logger = KotlinLogging.logger {}

suspend fun main() = coroutineScope {
    TataruBot(System.getenv("DISCORD_TOKEN")).connect()
}