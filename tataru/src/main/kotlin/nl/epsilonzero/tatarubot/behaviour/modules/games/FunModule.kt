package nl.epsilonzero.tatarubot.behaviour.modules.games

import nl.epsilonzero.tatarubot.framework.ModuleBuilder
import nl.epsilonzero.tatarubot.framework.ModulesBuilder
import nl.epsilonzero.tatarubot.framework.common.effects.removeMessage
import nl.epsilonzero.tatarubot.framework.common.effects.respond
import java.text.DecimalFormat
import kotlin.math.absoluteValue

fun ModulesBuilder<*, *>.games() = module("games") {
    ship()
}

fun ModuleBuilder<*, *>.ship() = command("ship") { args ->
    val participants = args
        .filter { it.isNotBlank() && it.startsWith("<@") }
        .distinct()
        .sorted()

    if (participants.isEmpty()) {
        respond(args) { +"Ship. Noun. A large floating vessel for transporting people or goods by sea." }
        return@command
    }

    if (participants.size < 2) {
        respond(args) { +"Sorry, I can't ship you with yourself." }
        return@command
    }

    val shippingScore = calculateShipPercentage(participants)

    respond(args) {
        +"My Arcanist handbook says you are about ${scoreFormat.format(shippingScore)} compatible."
    }

    removeMessage(args.message)
}

private val scoreFormat = DecimalFormat("##.##%")
fun calculateShipPercentage(names: List<String>): Double {
    return names.map { it.hashCode().absoluteValue }.sum().absoluteValue % 1000 / 1000.0
}