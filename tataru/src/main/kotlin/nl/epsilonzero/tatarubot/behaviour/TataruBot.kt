import nl.epsilonzero.tatarubot.behaviour.modules.nekolife.nekosLife
import nl.epsilonzero.tatarubot.framework.Bot
import nl.epsilonzero.tatarubot.framework.discord.Discord

fun TataruBot(token: String) = Bot(Discord) {
    configuration {
        this.token = token
    }

    modules {
        nekosLife()
    }
}