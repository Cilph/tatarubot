package nl.epsilonzero.tatarubot.behaviour.modules.events

import kotlinx.coroutines.delay
import nl.epsilonzero.tatarubot.framework.ModuleBuilder
import nl.epsilonzero.tatarubot.framework.ModulesBuilder
import nl.epsilonzero.tatarubot.framework.common.effects.addReaction
import nl.epsilonzero.tatarubot.framework.common.effects.removeMessage
import nl.epsilonzero.tatarubot.framework.common.effects.respond
import nl.epsilonzero.tatarubot.framework.common.effects.waitForReaction
import nl.epsilonzero.tatarubot.framework.entities.UnicodeEmoji

private val emojiYes = UnicodeEmoji("✅")
private val emojiNo = UnicodeEmoji("❌")

fun ModulesBuilder<*, *>.eventManagement() = module("eventManagement") {
    suggest()
}

fun ModuleBuilder<*, *>.suggest() = command("suggest") { args ->
    if (args.size < 2) {
        respond(args) { +"Sorry, I need to know the 'when' and 'what' of your suggestion." }
    }
    val suggestionWhen = args[1]
    val suggestionWhat = args.drop(1).joinToString(" ")

    val verificationMessage = respond(args) { +"So, you want to '$suggestionWhat' on '$suggestionWhen'?" }
    addReaction(verificationMessage, emojiYes, emojiNo)

    val reactionTrigger = waitForReaction {
        it.messageId == verificationMessage.id && it.authorId == args.author?.id &&
                (it.emoji == emojiYes || it.emoji == emojiNo)
    }
    when (reactionTrigger.emoji) {
        emojiYes -> respond(args) { +"Your suggestion has been noted." }
        emojiNo -> respond(args) { +"Well uhh... okay. Try again!" }
    }

    removeMessage(args.message)
    delay(5000)
    removeMessage(verificationMessage)
}