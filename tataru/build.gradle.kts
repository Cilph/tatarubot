val ktorVersion: String by project
val jacksonVersion: String by project

plugins {
    application
    id("org.jlleitschuh.gradle.ktlint") version "6.3.1"
    id("nu.studer.jooq") version "3.0.2"
    kotlin("jvm")
}

tasks.withType<Test> {
    useJUnitPlatform()
    reports {
        junitXml.isEnabled = true
        html.isEnabled = true
    }
}

dependencies {
    implementation(project(":framework"))
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.2.1")
    implementation("io.ktor:ktor-client-core:1.2.2")
    implementation("io.ktor:ktor-client-cio:1.2.2")
    implementation("io.ktor:ktor-client-json:1.2.2")
    implementation("io.ktor:ktor-client-jackson:1.2.2")

    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("io.github.microutils:kotlin-logging:1.6.22")

    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.2.1")
    testImplementation("com.natpryce:hamkrest:1.7.0.0")
    testImplementation("io.mockk:mockk:1.9")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.3.2")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.3.2")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.3.2")
    testImplementation("org.spekframework.spek2:spek-dsl-jvm:2.0.5")
}
