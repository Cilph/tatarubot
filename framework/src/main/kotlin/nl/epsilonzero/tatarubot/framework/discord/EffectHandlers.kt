package nl.epsilonzero.tatarubot.framework.discord

import discord4j.core.`object`.entity.TextChannel
import discord4j.core.`object`.reaction.ReactionEmoji
import discord4j.core.`object`.util.Snowflake
import discord4j.core.event.domain.message.MessageCreateEvent
import discord4j.core.event.domain.message.ReactionAddEvent
import discord4j.core.spec.MessageCreateSpec
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import nl.epsilonzero.tatarubot.framework.BotCoroutine
import nl.epsilonzero.tatarubot.framework.common.config.CommandPrefix
import nl.epsilonzero.tatarubot.framework.common.config.GetConfigValue
import nl.epsilonzero.tatarubot.framework.common.effects.AddReaction
import nl.epsilonzero.tatarubot.framework.common.effects.Effect
import nl.epsilonzero.tatarubot.framework.common.effects.Fork
import nl.epsilonzero.tatarubot.framework.common.effects.RemoveMessage
import nl.epsilonzero.tatarubot.framework.common.effects.RemoveReaction
import nl.epsilonzero.tatarubot.framework.common.effects.ResolveUser
import nl.epsilonzero.tatarubot.framework.common.effects.SendMessage
import nl.epsilonzero.tatarubot.framework.common.effects.WaitForMessage
import nl.epsilonzero.tatarubot.framework.common.effects.WaitForReaction
import nl.epsilonzero.tatarubot.framework.entities.MessageBuilder
import nl.epsilonzero.tatarubot.framework.entities.UnicodeEmoji
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.ofType

interface DiscordEffectHandler<E, R> {
    val key: Effect.Key<E, R>
    fun handle(sub: Discord.Subscriber, effect: E): Mono<R>
}

private inline fun <E, R> discordEffectHandler(
    key: Effect.Key<E, R>,
    crossinline body: (Discord.Subscriber, E) -> Mono<R>
): DiscordEffectHandler<E, R> {
    return object : DiscordEffectHandler<E, R> {
        override val key = key
        override fun handle(sub: Discord.Subscriber, effect: E) = body(sub, effect)
    }
}

val WaitForMessageHandler = discordEffectHandler(WaitForMessage) { sub, effect ->
    sub.bufferedEventFlux
        .ofType<MessageCreateEvent>()
        .filter { it.message.timestamp > sub.lastMessageProcessed }
        .map { it.message.toCommon() }
        .filter { effect.filter(it) }
        .take(1)
        .single()
        .doOnNext {
            sub.lastMessageProcessed = it.timestamp
        }
}

val WaitForReactionHandler = discordEffectHandler(WaitForReaction) { sub, effect ->
    sub.bufferedEventFlux
        .ofType<ReactionAddEvent>()
        .filter { effect.filter(it.toCommon()) }
        .take(1)
        .single()
        .map { it.toCommon() }
}

val SendMessageHandler = discordEffectHandler(SendMessage) { sub, effect ->
    fun MessageCreateSpec.fromCommon(builder: MessageBuilder) {
        setContent(builder.content ?: "")
        val embedBuilder = builder.embed
        if (embedBuilder != null) {
            with(embedBuilder) {
                setEmbed { discordEmbed ->
                    timestamp?.let { discordEmbed.setTimestamp(it) }
                    title?.let { discordEmbed.setTitle(it) }
                    author?.let { discordEmbed.setAuthor(it, authorUrl, authorIconUrl) }
                    description?.let { discordEmbed.setDescription(it) }
                    url?.let { discordEmbed.setUrl(it) }
                    imageUrl?.let { discordEmbed.setImage(it) }
                    color?.let { discordEmbed.setColor(it) }
                    footer?.let { discordEmbed.setFooter(it, footerIconUrl) }
                    thumbnailUrl?.let { discordEmbed.setThumbnail(it) }
                }
            }
        }
    }

    sub.client.getChannelById(Snowflake.of(effect.channelId))
        .ofType(TextChannel::class.java)
        .flatMap { it.createMessage { spec -> spec.fromCommon(effect.message) } }
        .map { it.toCommon() }
}

val RemoveMessageHandler = discordEffectHandler(RemoveMessage) { sub, effect ->
    sub.client.getMessageById(Snowflake.of(effect.channelId), Snowflake.of(effect.messageId))
        .flatMap { it.delete() }
        .map { Unit }
}

val AddReactionHandler = discordEffectHandler(AddReaction) { sub, effect ->
    val emoji = effect.emoji.map {
        when (it) {
            is UnicodeEmoji -> ReactionEmoji.unicode(it.unicode)
            else -> TODO()
        }
    }

    sub.client.getMessageById(Snowflake.of(effect.channelId), Snowflake.of(effect.messageId))
        .flatMapMany { message -> Flux.merge(emoji.map { message.addReaction(it) }) }
        .last()
        .map { Unit }
}

val RemoveReactionHandler = discordEffectHandler(RemoveReaction) { sub, effect ->
    sub.client.getMessageById(Snowflake.of(effect.channelId), Snowflake.of(effect.messageId))
        .flatMap { it.delete() }
        .map { Unit }
}

val ResolveUser = discordEffectHandler(ResolveUser) { sub, effect ->
    sub.client.getUserById(Snowflake.of(effect.userId))
        .map { it.toCommon() }
}

val GetConfigValue = discordEffectHandler(GetConfigValue) { sub, effect ->
    val retval: Mono<Any> = when (effect.configKey) {
        CommandPrefix -> Mono.just("t")
        else -> throw IllegalStateException()
    }

    retval
}

@Suppress("UNCHECKED_CAST")
val Fork = discordEffectHandler(Fork) { sub, effect ->
    val discordFork = effect as Fork<Discord>
    val coroutine = BotCoroutine<Discord>(
        self = sub.self,
        effectHandler = sub.fork()
    )

    Mono.just(GlobalScope.launch {
        discordFork.saga(coroutine)
    })
}