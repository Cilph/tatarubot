package nl.epsilonzero.tatarubot.framework

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import nl.epsilonzero.tatarubot.framework.entities.Message
import java.lang.ref.WeakReference

val noCommandLocking: suspend (String, Message) -> Mutex? = { _, _ -> null }

private val channelLockMutex = Mutex()
private val channelLockMap = mutableMapOf<Pair<String, String>, WeakReference<Mutex>>()
val oncePerChannel: suspend (String, Message) -> Mutex? = { command, message ->
    val key = command to message.channelId
    channelLockMap[key]?.get() ?: channelLockMutex.withLock {
        val ref = channelLockMap.getOrPut(key) { WeakReference(Mutex()) }
        ref.get()
    }
}