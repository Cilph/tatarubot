package nl.epsilonzero.tatarubot.framework.common.effects

import kotlinx.coroutines.Job
import nl.epsilonzero.tatarubot.framework.BotScope

data class Fork<E>(val saga: suspend BotScope<E>.() -> Unit) : Effect<Fork<E>, Job> {
    @Suppress("UNCHECKED_CAST")
    override val key = Key as Effect.Key<Fork<E>, Job>
    companion object Key : Effect.Key<Fork<*>, Job>
}

suspend fun <Engine> BotScope<Engine>.spawn(body: suspend BotScope<Engine>.() -> Unit): Job {
    return yield(Fork(body))
}