package nl.epsilonzero.tatarubot.framework.common.effects

import nl.epsilonzero.tatarubot.framework.BotScope
import nl.epsilonzero.tatarubot.framework.entities.Message

data class RemoveMessage(val channelId: String, val messageId: String) :
    Effect<RemoveMessage, Unit> {
    override val key = Key
    companion object Key :
        Effect.Key<RemoveMessage, Unit>
}

suspend fun BotScope<*>.removeMessage(message: Message) {
    return yield(RemoveMessage(message.channelId, message.id))
}