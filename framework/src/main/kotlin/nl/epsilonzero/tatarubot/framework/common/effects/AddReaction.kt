package nl.epsilonzero.tatarubot.framework.common.effects

import nl.epsilonzero.tatarubot.framework.BotScope
import nl.epsilonzero.tatarubot.framework.entities.Emoji
import nl.epsilonzero.tatarubot.framework.entities.Message

data class AddReaction(val channelId: String, val messageId: String, val emoji: List<Emoji>) :
    Effect<AddReaction, Unit> {
    override val key = Key
    companion object Key :
        Effect.Key<AddReaction, Unit>

    constructor(message: Message, emoji: Emoji) : this(message.channelId, message.id, listOf(emoji))
    constructor(message: Message, emoji: List<Emoji>) : this(message.channelId, message.id, emoji)
}

suspend fun BotScope<*>.addReaction(message: Message, vararg emoji: Emoji) {
    return yield(AddReaction(message, emoji.toList()))
}

suspend fun BotScope<*>.addReaction(message: Message, emoji: List<Emoji>) {
    return yield(AddReaction(message, emoji))
}