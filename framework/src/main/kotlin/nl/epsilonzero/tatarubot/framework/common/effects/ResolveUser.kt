package nl.epsilonzero.tatarubot.framework.common.effects

import nl.epsilonzero.tatarubot.framework.BotScope
import nl.epsilonzero.tatarubot.framework.entities.User

data class ResolveUser(val userId: String) : Effect<ResolveUser, User?> {
    override val key = Key

    companion object Key : Effect.Key<ResolveUser, User?>
}

suspend fun BotScope<*>.resolveUser(userId: String): User? {
    return yield(ResolveUser(userId))
}