package nl.epsilonzero.tatarubot.framework.common.config

import nl.epsilonzero.tatarubot.framework.BotScope
import nl.epsilonzero.tatarubot.framework.common.effects.Effect

data class GetConfigValue<K : ConfigKey<V>, V>(val configKey: K) :
    Effect<GetConfigValue<K, V>, V> {
    @Suppress("UNCHECKED_CAST")
    override val key = Key as Effect.Key<GetConfigValue<K, V>, V>

    companion object Key :
        Effect.Key<GetConfigValue<*, *>, Any>
}

suspend fun <K : ConfigKey<V>, V> BotScope<*>.config(key: K): V {
    return yield(GetConfigValue(key))
}

suspend fun <K : ConfigKey<V>, V> BotScope<*>.config(key: K, value: V): V {
    return yield(SetConfigValue(key, value))
}