package nl.epsilonzero.tatarubot.framework.common.effects

interface Effect<Self, ReturnType> {
    interface Key<S, R>
    val key: Key<Self, ReturnType>
}

data class RemoveReaction(
    val channelId: String,
    val messageId: String,
    val emojiId: String,
    val emojiName: String
) : Effect<RemoveReaction, Unit> {
    override val key = Key
    companion object Key :
        Effect.Key<RemoveReaction, Unit>
}