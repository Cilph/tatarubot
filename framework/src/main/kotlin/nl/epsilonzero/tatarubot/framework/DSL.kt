package nl.epsilonzero.tatarubot.framework

import nl.epsilonzero.tatarubot.framework.entities.CommandInvocation
import nl.epsilonzero.tatarubot.framework.entities.Message

interface Bot {
    suspend fun connect()
    fun disconnect()
}

interface BotEngine<T, C> {
    fun create(body: BotBuilder<T, C>.() -> Unit): Bot
}

interface BotBuilder<Type, Configuration> {
    data class CommonConfiguration(var commandPrefix: String)

    fun configuration(init: Configuration.() -> Unit)
    fun modules(body: ModulesBuilder<Type, Configuration>.() -> Unit)
}

interface ModulesBuilder<Type, Configuration> {
    fun module(name: String, body: ModuleBuilder<Type, Configuration>.() -> Unit)
}

interface ModuleBuilder<Type, Configuration> {
    fun command(name: String? = null, body: suspend BotScope<Type>.(CommandInvocation) -> Unit)
    fun saga(name: String? = null, body: suspend BotScope<Type>.() -> Unit)
    fun interceptor(body: (Message) -> Boolean)
}

fun <T, C> Bot(type: BotEngine<T, C>, init: BotBuilder<T, C>.() -> Unit): Bot {
    return type.create(init)
}
