package nl.epsilonzero.tatarubot.framework.discord

import nl.epsilonzero.tatarubot.framework.common.effects.Effect

class EffectHandlerRegistry(handlers: Iterable<DiscordEffectHandler<*, *>>) {
    private val handlers: Map<Effect.Key<*, *>, DiscordEffectHandler<*, *>> = handlers.associate { it.key to it }

    operator fun <E, R> get(key: Effect.Key<E, R>): DiscordEffectHandler<E, R>? {
        @Suppress("UNCHECKED_CAST")
        return handlers[key] as DiscordEffectHandler<E, R>?
    }
}