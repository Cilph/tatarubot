package nl.epsilonzero.tatarubot.framework

import nl.epsilonzero.tatarubot.framework.common.effects.Effect
import nl.epsilonzero.tatarubot.framework.entities.Message
import nl.epsilonzero.tatarubot.framework.entities.User

interface BotScope<Type> {
    val self: User

    suspend fun <E : Effect<E, R>, R> yield(effect: E): R
}

infix fun Message.isCommand(command: String): Boolean {
    return content.firstWord() resembles command
}

infix fun Message.resembles(content: String): Boolean = this.content resembles content
infix fun String.resembles(other: String): Boolean {
    fun String.loosen() = this.toLowerCase().replace(Regex("[^a-z0-9]"), "")

    return this.loosen() == other.loosen()
}

fun String.firstWord(): String {
    val firstSpace = this.indexOfFirst { it == ' ' }
    return if (firstSpace == -1) {
        this
    } else {
        this.substring(0, firstSpace)
    }
}
