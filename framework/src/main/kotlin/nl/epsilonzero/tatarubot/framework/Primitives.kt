package nl.epsilonzero.tatarubot.framework

import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.suspendCancellableCoroutine
import nl.epsilonzero.tatarubot.framework.common.effects.Effect
import nl.epsilonzero.tatarubot.framework.entities.User

interface EffectHandler : AutoCloseable {
    fun <E : Effect<E, R>, R> handle(effect: E, continuation: CancellableContinuation<R>)
}

class BotCoroutine<Engine>(
    override val self: User,
    private val effectHandler: EffectHandler
) : BotScope<Engine> {
    override suspend fun <E : Effect<E, R>, R> yield(effect: E): R {
        return suspendCancellableCoroutine { cont ->
            effectHandler.handle(effect, cont)
        }
    }
}