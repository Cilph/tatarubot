package nl.epsilonzero.tatarubot.framework.common.config

import nl.epsilonzero.tatarubot.framework.common.effects.Effect

data class SetConfigValue<K : ConfigKey<V>, V>(val configKey: K, val value: V) :
    Effect<SetConfigValue<K, V>, V> {
    @Suppress("UNCHECKED_CAST")
    override val key = Key as Effect.Key<SetConfigValue<K, V>, V>
    companion object Key :
        Effect.Key<SetConfigValue<*, *>, Any>
}