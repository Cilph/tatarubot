package nl.epsilonzero.tatarubot.framework.entities

interface Emoji

data class UnicodeEmoji(val unicode: String) : Emoji
object UnknownEmoji : Emoji