package nl.epsilonzero.tatarubot.framework.common.config

interface ConfigKey<V>

object CommandPrefix : ConfigKey<String>
