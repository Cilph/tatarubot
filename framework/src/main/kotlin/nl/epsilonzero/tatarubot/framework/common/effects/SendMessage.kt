package nl.epsilonzero.tatarubot.framework.common.effects

import nl.epsilonzero.tatarubot.framework.entities.Message
import nl.epsilonzero.tatarubot.framework.entities.MessageBuilder

data class SendMessage(val channelId: String, val message: MessageBuilder) :
    Effect<SendMessage, Message> {
    override val key = Key
    companion object Key :
        Effect.Key<SendMessage, Message>
}