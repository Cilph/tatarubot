package nl.epsilonzero.tatarubot.framework

import kotlinx.coroutines.suspendCancellableCoroutine
import org.reactivestreams.Publisher
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

@Suppress("SubscriberImplementation")
suspend fun <T> Publisher<T>.awaitCompletion(): Unit = suspendCancellableCoroutine { cont ->
    subscribe(object : Subscriber<T> {
        private lateinit var subscription: Subscription

        override fun onNext(t: T) {}

        override fun onSubscribe(sub: Subscription) {
            subscription = sub
            cont.invokeOnCancellation { sub.cancel() }
            sub.request(1)
        }

        override fun onComplete() {
            cont.resume(Unit)
        }

        override fun onError(e: Throwable) {
            cont.resumeWithException(e)
        }
    })
}