package nl.epsilonzero.tatarubot.framework.entities

import java.awt.Color
import java.time.Instant

data class Message(
    val id: String,
    val channelId: String,
    val author: User?,
    val content: String,
    val timestamp: Instant,
    val userMentionIds: List<String>
)

class MessageBuilder(
    var content: String? = null,
    var embed: EmbedBuilder? = null
) {
    fun embed(body: EmbedBuilder.() -> Unit) {
        embed = EmbedBuilder().apply { body() }
    }

    operator fun String.unaryPlus() {
        content = this
    }
}

class EmbedBuilder(
    var timestamp: Instant? = null,
    var title: String? = null,
    var author: String? = null,
    var authorUrl: String? = null,
    var authorIconUrl: String? = null,
    var description: String? = null,
    var url: String? = null,
    var imageUrl: String? = null,
    var color: Color? = null,
    var footer: String? = null,
    var footerIconUrl: String? = null,
    var thumbnailUrl: String? = null
)