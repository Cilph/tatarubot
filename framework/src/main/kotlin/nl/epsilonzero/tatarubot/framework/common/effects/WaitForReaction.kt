package nl.epsilonzero.tatarubot.framework.common.effects

import nl.epsilonzero.tatarubot.framework.BotScope
import nl.epsilonzero.tatarubot.framework.entities.Reaction

data class WaitForReaction(val filter: (Reaction) -> Boolean) :
    Effect<WaitForReaction, Reaction> {
    override val key = Key
    companion object Key :
        Effect.Key<WaitForReaction, Reaction>
}

suspend fun BotScope<*>.waitForReaction(filter: (Reaction) -> Boolean): Reaction {
    return yield(WaitForReaction(filter))
}