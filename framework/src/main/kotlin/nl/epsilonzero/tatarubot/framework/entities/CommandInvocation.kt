package nl.epsilonzero.tatarubot.framework.entities

data class CommandInvocation(
    val message: Message,
    val prefix: String,
    val command: String,
    val arguments: List<String>
) : List<String> by arguments {
    val timestamp get() = message.timestamp
    val author get() = message.author
    val channelId get() = message.channelId
    val userMentionIds get() = message.userMentionIds
}

private val argPattern = Regex("([^\"]\\S*|\".+?\")\\s*")
fun Message.asCommandInvocation(prefix: String): CommandInvocation {
    require(content.startsWith(prefix))

    val args = argPattern.findAll(content)
        .map { it.value.replace("\"", "") }
        .toList()

    return CommandInvocation(
        message = this,
        prefix = prefix,
        command = args[0].removePrefix(prefix),
        arguments = args.drop(1)
    )
}