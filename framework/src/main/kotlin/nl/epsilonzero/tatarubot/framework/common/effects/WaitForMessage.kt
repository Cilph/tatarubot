package nl.epsilonzero.tatarubot.framework.common.effects

import nl.epsilonzero.tatarubot.framework.BotScope
import nl.epsilonzero.tatarubot.framework.entities.CommandInvocation
import nl.epsilonzero.tatarubot.framework.entities.Message
import nl.epsilonzero.tatarubot.framework.entities.MessageBuilder

data class WaitForMessage(val filter: (Message) -> Boolean) :
    Effect<WaitForMessage, Message> {
    override val key = Key
    companion object Key :
        Effect.Key<WaitForMessage, Message>
}

suspend fun BotScope<*>.waitForMessage(filter: (Message) -> Boolean): Message {
    return yield(WaitForMessage(filter))
}

suspend fun BotScope<*>.respond(message: Message, messageBuilder: MessageBuilder.() -> Unit): Message {
    return yield(
        SendMessage(
            message.channelId,
            MessageBuilder().apply { messageBuilder() })
    )
}

suspend fun BotScope<*>.respond(
    command: CommandInvocation,
    messageBuilder: MessageBuilder.() -> Unit
) = respond(command.message, messageBuilder)