package nl.epsilonzero.tatarubot.framework.discord

import discord4j.core.DiscordClient
import discord4j.core.DiscordClientBuilder
import discord4j.core.event.EventDispatcher
import discord4j.core.event.domain.Event
import discord4j.core.event.domain.lifecycle.ReadyEvent
import discord4j.core.event.domain.message.ReactionAddEvent
import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.reactor.mono
import kotlinx.coroutines.sync.Mutex
import nl.epsilonzero.tatarubot.framework.Bot
import nl.epsilonzero.tatarubot.framework.BotBuilder
import nl.epsilonzero.tatarubot.framework.BotCoroutine
import nl.epsilonzero.tatarubot.framework.BotEngine
import nl.epsilonzero.tatarubot.framework.BotScope
import nl.epsilonzero.tatarubot.framework.EffectHandler
import nl.epsilonzero.tatarubot.framework.ModuleBuilder
import nl.epsilonzero.tatarubot.framework.ModulesBuilder
import nl.epsilonzero.tatarubot.framework.awaitCompletion
import nl.epsilonzero.tatarubot.framework.common.config.CommandPrefix
import nl.epsilonzero.tatarubot.framework.common.config.config
import nl.epsilonzero.tatarubot.framework.common.effects.Effect
import nl.epsilonzero.tatarubot.framework.common.effects.spawn
import nl.epsilonzero.tatarubot.framework.common.effects.waitForMessage
import nl.epsilonzero.tatarubot.framework.entities.CommandInvocation
import nl.epsilonzero.tatarubot.framework.entities.Emoji
import nl.epsilonzero.tatarubot.framework.entities.Message
import nl.epsilonzero.tatarubot.framework.entities.Reaction
import nl.epsilonzero.tatarubot.framework.entities.UnicodeEmoji
import nl.epsilonzero.tatarubot.framework.entities.UnknownEmoji
import nl.epsilonzero.tatarubot.framework.entities.User
import nl.epsilonzero.tatarubot.framework.entities.asCommandInvocation
import nl.epsilonzero.tatarubot.framework.isCommand
import nl.epsilonzero.tatarubot.framework.noCommandLocking
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Instant
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class Discord(
    private val config: Configuration,
    private val commands: List<Command>,
    private val sagas: List<Saga>,
    private val interceptors: List<Interceptor>
) : Bot {
    val client = DiscordClientBuilder(config.token)
        .build()

    private val effectHandlers = EffectHandlerRegistry(
        listOf(
            WaitForMessageHandler,
            WaitForReactionHandler,
            SendMessageHandler,
            RemoveMessageHandler,
            AddReactionHandler,
            RemoveReactionHandler,
            ResolveUser,
            Fork,
            GetConfigValue
        )
    )

    override suspend fun connect() = coroutineScope {
        client.eventDispatcher.on<ReadyEvent>()
            .map { it.self.toCommon() }
            .flatMap { u -> mono { main(u) } }
            .zipWith(client.login())
            .awaitCompletion()
    }

    override fun disconnect() {
        client.logout().block()
    }

    private suspend fun main(self: User) {
        val eventStream = client.eventDispatcher
            .on(discord4j.core.event.domain.Event::class.java)
            .publish().autoConnect()

        BotCoroutine<Discord>(
            self = self,
            effectHandler = Subscriber(client, self, Instant.MIN, eventStream)
        ).root()
    }

    private suspend fun BotScope<Discord>.root() {
        commands.forEach {
            spawn { commandSaga(it.name, noCommandLocking, it.body) }
        }
    }

    inner class Subscriber(
        val client: DiscordClient,
        val self: User,
        var lastMessageProcessed: Instant = Instant.MIN,
        val bufferedEventFlux: Flux<Event>
    ) : EffectHandler, AutoCloseable {
        override fun <E : Effect<E, R>, R> handle(effect: E, continuation: CancellableContinuation<R>) {
            handleEffect(effect)
                .doOnError { continuation.resumeWithException(it) }
                .doOnCancel { continuation.cancel(CancellationException("")) }
                .doOnNext { continuation.resume(it as R) }
                .subscribe()
        }

        private fun <E : Effect<E, R>, R> handleEffect(effect: E): Mono<R> {
            val handler = effectHandlers[effect.key] ?: throw IllegalStateException("no handler")
            return handler.handle(this, effect)
        }

        fun fork(): Subscriber {
            return Subscriber(client, self, lastMessageProcessed, bufferedEventFlux)
        }

        override fun close() {
        }
    }

    data class Module(val name: String)
    data class Command(
        val name: String,
        val module: Module?,
        val body: suspend BotScope<Discord>.(CommandInvocation) -> Unit
    )

    data class Saga(val name: String, val module: Module?, val body: suspend BotScope<Discord>.() -> Unit)
    data class Interceptor(val name: String, val module: Module?, val body: (Message) -> Boolean)

    data class Configuration(
        var token: String = "<unset>"
    )

    companion object Type : BotEngine<Discord, Configuration> {
        override fun create(body: BotBuilder<Discord, Configuration>.() -> Unit): Bot {
            val configuration = Configuration()
            val commands = mutableListOf<Command>()
            val sagas = mutableListOf<Saga>()
            val interceptors = mutableListOf<Interceptor>()

            fun moduleBuilder(module: Module) = object : ModuleBuilder<Discord, Configuration> {
                override fun saga(name: String?, body: suspend BotScope<Discord>.() -> Unit) {
                    sagas += Saga(name ?: "", module, body)
                }

                override fun interceptor(body: (Message) -> Boolean) {
                    interceptors += Interceptor("", module, body)
                }

                override fun command(name: String?, body: suspend BotScope<Discord>.(CommandInvocation) -> Unit) {
                    commands += Command(name ?: "", module, body)
                }
            }

            fun modulesBuilder() = object : ModulesBuilder<Discord, Configuration> {
                override fun module(name: String, body: ModuleBuilder<Discord, Configuration>.() -> Unit) {
                    moduleBuilder(Module(name)).body()
                }
            }

            fun botBuilder() = object : BotBuilder<Discord, Configuration> {
                override fun configuration(init: Configuration.() -> Unit) {
                    configuration.init()
                }

                override fun modules(body: ModulesBuilder<Discord, Configuration>.() -> Unit) {
                    modulesBuilder().body()
                }
            }

            botBuilder().body()

            return Discord(
                configuration,
                commands,
                sagas,
                interceptors
            )
        }
    }
}

inline fun <reified T : Event> EventDispatcher.on() = this.on(T::class.java)

fun discord4j.core.`object`.entity.User.toCommon() = User(
    id = id.asString(),
    userName = username,
    mention = mention
)

fun discord4j.core.`object`.entity.Message.toCommon() = Message(
    id = id.asString(),
    channelId = channelId.asString(),
    author = author.map { it.toCommon() }.orElse(null),
    content = content.orElse(""),
    timestamp = timestamp,
    userMentionIds = userMentionIds.map { it.asString() }
)

fun ReactionAddEvent.toCommon() = Reaction(
    channelId = this.channelId.asString(),
    authorId = this.userId.asString(),
    messageId = this.messageId.asString(),
    emoji = this.emoji.asUnicodeEmoji().map<Emoji> { UnicodeEmoji(it.raw) }.orElse(UnknownEmoji)
)

suspend fun BotScope<Discord>.commandSaga(
    command: String,
    lockStrategy: suspend (String, Message) -> Mutex? = noCommandLocking,
    body: suspend BotScope<Discord>.(CommandInvocation) -> Unit
) {
    while (true) {
        val prefix = config(CommandPrefix)
        val trigger = waitForMessage { it isCommand "$prefix$command" }
        val lock = lockStrategy(command, trigger)
        lock?.tryLock(command)
        try {
            val fork = spawn {
                val invocation = trigger.asCommandInvocation(prefix)
                body(this, invocation)
            }
            fork.join()
        } finally {
            lock?.unlock(command)
        }
    }
}