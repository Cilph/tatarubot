package nl.epsilonzero.tatarubot.framework.entities

data class User(
    val id: String,
    val userName: String,
    val mention: String
)