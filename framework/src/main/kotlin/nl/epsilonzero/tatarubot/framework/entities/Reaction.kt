package nl.epsilonzero.tatarubot.framework.entities

data class Reaction(
    val channelId: String,
    val authorId: String,
    val messageId: String,
    val emoji: Emoji
)
