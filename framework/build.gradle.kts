import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val ktorVersion: String by project
val jacksonVersion: String by project

plugins {
    application
    id("org.jlleitschuh.gradle.ktlint") version "6.3.1"
    id("nu.studer.jooq") version "3.0.2"
    kotlin("jvm")
}

tasks.withType<Test> {
    useJUnitPlatform()
    reports {
        junitXml.isEnabled = true
        html.isEnabled = true
    }
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.2.1")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:1.2.1")

    implementation("com.discord4j:discord4j-core:3.0.6")

    implementation("ch.qos.logback:logback-classic:1.2.3")
    implementation("io.github.microutils:kotlin-logging:1.6.22")

    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.2.1")
    testImplementation("com.natpryce:hamkrest:1.7.0.0")
    testImplementation("io.mockk:mockk:1.9")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.3.2")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.3.2")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.3.2")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = freeCompilerArgs + "-Xuse-experimental=kotlin.experimental.ExperimentalTypeInference"
        freeCompilerArgs = freeCompilerArgs + "-Xuse-experimental=kotlin.contracts.ExperimentalContracts"
        freeCompilerArgs = freeCompilerArgs + "-Xuse-experimental=kotlinx.coroutines.InternalCoroutinesApi"
    }
}