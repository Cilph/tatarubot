import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    base
    id("idea")
    kotlin("jvm") version "1.3.30" apply false
}

allprojects {
    group = "nl.epsilonzero.tatarubot"
    version = "0.1"

    repositories {
        jcenter()
    }
}

subprojects {
    tasks.withType<KotlinCompile>().configureEach {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }

    tasks.withType<AbstractArchiveTask>().configureEach {
        archiveFileName.set("${project.name}.${archiveExtension.get()}")
    }

    tasks.withType<KotlinCompile>().all {
        kotlinOptions.freeCompilerArgs += "-Xuse-experimental=kotlinx.coroutines.ObsoleteCoroutinesApi"
        kotlinOptions.freeCompilerArgs += "-Xuse-experimental=kotlinx.coroutines.ExperimentalCoroutinesApi"
//        kotlinOptions.freeCompilerArgs += "-XXLanguage:+InlineClasses"
    }
}
